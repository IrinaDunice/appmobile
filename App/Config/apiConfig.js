import apisauce, {create} from 'apisauce'
export const api = create({
  baseURL: 'http://192.168.1.98:3000/',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'X-Even-More': 'hawtness',
  }
})
