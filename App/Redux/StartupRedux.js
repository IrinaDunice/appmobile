import {createActions, createReducer} from 'reduxsauce'

/* ------------- Types and Action Creators ------------- */
const {Types, Creators} = createActions({
  startup: null,
  getUserRequest: null,
  getUserSuccess: ['user'],
  getUserFailure: ['error']
}, {})

export const StartupTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = {
  fetching: false,
  currentUser: {},
  error: null
}
/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state = INITIAL_STATE, action) => {
  return Object.assign({}, state, {
    fetching: true
  })
}
export const success = (state = INITIAL_STATE, action) => {
  console.log('action', action)
  return Object.assign({}, state, {
    fetching: false,
    currentUser: action.user
  })
}
export const failure = (state = INITIAL_STATE, action) => {
  console.log('action', action)
  return Object.assign({}, state, {
    fetching: false,
    error: action.error
  })
}

export const HANDLERS = {
  [Types.GET_USER_REQUEST]: request,
  [Types.GET_USER_SUCCESS]: success,
  [Types.GET_USER_FAILURE]: failure,
}

export const reducer = createReducer(INITIAL_STATE, HANDLERS)
