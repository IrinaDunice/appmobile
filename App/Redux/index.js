import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'
import AppNavigation from '../Navigation/AppNavigation'
const navReducer = (state, action) => {
  const newState = AppNavigation.router.getStateForAction(action, state)
  return newState || state
}
export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    github: require('./GithubRedux').reducer,
    login: require('./LoginRedux').reducer,
    signup: require('./SignupRedux').reducer,
    search: require('./SearchRedux').reducer,
    profile: require('./ProfileRedux').reducer,
    users: require('./UsersRedux').reducer,
    startup: require('./StartupRedux').reducer,
    nav: navReducer
  })

  return configureStore(rootReducer, rootSaga)
}
