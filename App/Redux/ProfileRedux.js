import {createReducer, createActions} from 'reduxsauce'
import Immutable from 'seamless-immutable'
import {AsyncStorage} from 'react-native';

import {api} from '../Config/apiConfig'
/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  fetchUserRequest: ['userId'],
  fetchUserSuccess: ['response'],
  fetchUserFailure: ['error'],
})

export const ProfileTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  user: {},
  error: null,
  fetching: false
})

/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state = INITIAL_STATE, action) => {
  return state.merge({fetching: true})
}

// we've successfully logged in
export const success = (state = INITIAL_STATE, action) => {
  console.log('action', action)
 return ({...state, user: action.data.user, fetching: false})
}

// we've had a problem logging in
export const failure = (state, {error}) =>
  state.merge({fetching: false, error})


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.FETCH_USER_REQUEST]: request,
  [Types.FETCH_USER_SUCCESS]: success,
  [Types.FETCH_USER_FAILURE]: failure,
})
