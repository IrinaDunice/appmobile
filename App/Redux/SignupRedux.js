import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import apisauce, { create } from 'apisauce'

const api = create({
  baseURL: 'http://192.168.1.98:3000',
  headers: {'Cache-Control': 'no-cache'}
})
/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  signupRequest: ['user'],
  signupSuccess:  ['user'],
  signupFailure: ['error'],
})

export const SignupTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  user: {
    username: '',
    firstname: '',
    lastname: '',
    email: '',
    password: null,
  },
  error: null,
  fetching: false
})

/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state = INITIAL_STATE, action) => {
  console.log('action request', action)
  return state.merge({ fetching: true })
}

// we've successfully logged in
export const success = async (state = INITIAL_STATE, action) => {
  const {user} = action
  console.log('action--', action)
  const response = await api.post('/signup', user)
  console.log('response--',response);
}

// we've had a problem logging in
export const failure = (state, {error}) =>
  state.merge({fetching: false, error})


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SIGNUP_REQUEST]: request,
  [Types.SIGNUP_SUCCESS]: success,
  [Types.SIGNUP_FAILURE]: failure,
})


