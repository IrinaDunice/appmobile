import {createReducer, createActions} from 'reduxsauce'
import Immutable from 'seamless-immutable'
// import apisauce, {create} from 'apisauce'
// import {AsyncStorage} from 'react-native';
//
// const api = create({
//   baseURL: 'http://192.168.1.98:3000',
//   headers: {'Cache-Control': 'no-cache'}
// })
/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  getUsersRequest: null,
  getUsersSuccess: ['users'],
  getUsersFailure: ['error'],
})

export const UsersTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  users: {},
  error: null,
  fetching: false
})

/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state = INITIAL_STATE, action) => {
  console.log('action', action)
  return state.merge({fetching: true})
}

// we've successfully logged in
export const success = (state = INITIAL_STATE, action) => {
  console.log('action', action)
  const {users} = action
  return Object.assign({}, state, {users:users, fetching: false});

}

// we've had a problem logging in
export const failure = (state, {error}) =>
  state.merge({fetching: false, error})


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_USERS_REQUEST]: request,
  [Types.GET_USERS_SUCCESS]: success,
  [Types.GET_USERS_FAILURE]: failure,
})
