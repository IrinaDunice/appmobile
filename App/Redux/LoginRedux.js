import {createReducer, createActions} from 'reduxsauce'
import Immutable from 'seamless-immutable'
import {AsyncStorage} from 'react-native';
import {api} from '../Config/apiConfig'
/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  loginRequest: ['username', 'password'],
  loginSuccess: ['response'],
  loginFailure: ['error'],
  logout: null
})

export const LoginTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  username: '',
  password: null,
  error: null,
  fetching: false
})

/* ------------- Reducers ------------- */

// we're attempting to login
export const request = (state, action) => {
  return state.merge({fetching: true})
}

// we've successfully logged in
export const success =(state, action) => {
  const token = action.response.data.token
  AsyncStorage.setItem('@Token', token)
  return ({ ...state, fetching: false})
}

// we've had a problem logging in
export const failure = (state, {error}) =>
  state.merge({fetching: false, error})

// we've logged out
export const logout = (state) => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN_REQUEST]: request,
  [Types.LOGIN_SUCCESS]: success,
  [Types.LOGIN_FAILURE]: failure,
  [Types.LOGOUT]: logout
})


/* ------------- Selectors ------------- */

// Is the current user logged in?
export const isLoggedIn = (loginState) => loginState.username !== null
