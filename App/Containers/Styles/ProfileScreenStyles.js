import {StyleSheet} from 'react-native'
import {Metrics, ApplicationStyles} from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain'
  },
  centered: {
    alignItems: 'center'
  },
  topAvatarContainer: {
    height: 150,
    overflow: 'hidden'
  },
  topAvatar: {
    width: '100%',
  },
  userInfoContainer: {
  },
  rowUser: {
    marginBottom: 5,
    backgroundColor: '#fff',
    padding: 10,
  },
  title:{
    textAlign:'center',
    fontSize: 20
  }
})
