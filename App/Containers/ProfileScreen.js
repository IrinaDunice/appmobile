import React, {PropTypes, Component} from 'react'
import {connect} from 'react-redux'
import ProfileActions from '../Redux/ProfileRedux'
import {
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  Image, Button,
  Keyboard,
  LayoutAnimation
} from 'react-native'
import generalStyles from './Styles/GeneralStyles'
import styles from './Styles/ProfileScreenStyles'
import {Images, Metrics} from '../Themes'

class ProfileScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fetch: false
    }
    console.log('this', this)
  }

  componentWillMount() {
    // this.props.fetchProfileInfo()
  }

  componentWillReceiveProps(newProps) {
    this.forceUpdate()
  }

  render() {
    const {currentUser} = this.props
    console.log('currentUser', currentUser)
    return (
      <ScrollView contentContainerStyle={{justifyContent: 'center'}}
                  style={[generalStyles.container, {height: this.state.visibleHeight}]}
                  keyboardShouldPersistTaps='always'>

        {this.props.fetching === false ? <View>
          <View style={styles.topAvatarContainer}>
            <Image source={Images.avatar} style={[styles.topAvatar]}/>
          </View>
          <View style={styles.userInfoContainer}>
            <Text>{currentUser.username}</Text>
            <Text>{currentUser.firstname}</Text>
            <Text>{currentUser.lastname}</Text>
            <Text>{currentUser.email}</Text>
          </View>
        </View> : <Text>Loading...</Text>
        }
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  console.log('state', state)
  return {
    fetching: state.login.fetching,
    currentUser: state.startup.currentUser
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    fetchProfileInfo: () => dispatch(ProfileActions.fetchUserRequest()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)
