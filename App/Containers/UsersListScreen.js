import React, {PropTypes, Component} from 'react'
import {connect} from 'react-redux'
import UsersActions from '../Redux/UsersRedux'
import {
  View,
  ScrollView,
  Text,
} from 'react-native'
import generalStyles from './Styles/GeneralStyles'
import styles from './Styles/ProfileScreenStyles'

class UsersListScreen extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    fetching: PropTypes.bool,
    users: PropTypes.array,
    attemptLogin: PropTypes.func,
  }

  constructor(props) {
    super(props)
    this.state = {}
  }

  componentWillMount() {
    this.props.getAllUsers()
  }

  componentWillReceiveProps(newProps) {
    this.forceUpdate()
  }

  render() {
    const {users} = this.props
    return (
      <ScrollView contentContainerStyle={{justifyContent: 'center'}}
                  style={[generalStyles.container, {height: this.state.visibleHeight}]}
                  keyboardShouldPersistTaps='always'>
        <Text style={styles.title}>Users</Text>
        {this.props.error &&
        <h1>{this.props.error}</h1>}
        {this.props.fetching === false ? <View>
          <View style={styles.userInfoContainer}>
            {users.length > 0 && users.map((item, i) => {
              return (
                <View style={styles.rowUser} key={i}>
                  <Text>{item.username}</Text>
                </View>
              )
            })}
          </View>
        </View> : <Text>Loading...</Text>
        }
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    fetching: state.users.fetching,
    users: state.users.users,
    error: state.users.error,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    getAllUsers: () => dispatch(UsersActions.getUsersRequest()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersListScreen)
