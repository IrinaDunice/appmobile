import React from 'react'
import {connect} from 'react-redux'
import {ScrollView, Text, Image, View, Button} from 'react-native'
import {Images} from '../Themes'
import {AsyncStorage} from 'react-native';
import StartupActions from '../Redux/StartupRedux'
import FullButton from '../Components/FullButton'

// Styles
import styles from './Styles/LaunchScreenStyles'

class LaunchScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentWillMount() {
    let token = !!AsyncStorage.getItem('@Token')
    console.log('token',token)
    if (token) {
      this.props.getCurrenUser()
    }
    // if (this.props.fetching === false && this.props.currentUser) {
    //   this.props.navigation.navigate('ProfileScreen')
    // }
  }


  render() {

    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch'/>
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.launch} style={styles.logo}/>
          </View>
          <View style={styles.section}>
            <Text style={styles.sectionText}>
              This probably isn't what your app is going to look like.
            </Text>
          </View>
          <FullButton
            onPress={() => this.props.navigation.navigate('LoginScreen')}
            text="Login"
          />
          <FullButton
            onPress={() => this.props.navigation.navigate('ProfileScreen')}
            text="Profile"
          />
        </ScrollView>
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    fetching: state.startup.fetching,
    currentUser: state.startup.currentUser
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCurrenUser: () => dispatch(StartupActions.getUserRequest())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)
