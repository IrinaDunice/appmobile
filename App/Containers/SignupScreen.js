import React, {PropTypes} from 'react'
import {
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  Keyboard,
  LayoutAnimation
} from 'react-native'
import {connect} from 'react-redux'
import styles from './Styles/LoginScreenStyles'
import {Images, Metrics} from '../Themes'
import SignupActions from '../Redux/SignupRedux'

class SignupScreen extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func,
    fetching: PropTypes.bool,
    attemptSignup: PropTypes.func,
  }

  isAttempting = false
  keyboardDidShowListener = {}
  keyboardDidHideListener = {}

  constructor(props) {
    super(props)
    this.state = {
      username: 'Irenese',
      firstname: 'Irina',
      lastname: 'Martyshko',
      email: 'reactnative@infinite.red',
      password: 'password',
      visibleHeight: Metrics.screenHeight,
      topLogo: {width: Metrics.screenWidth}
    }
    this.isAttempting = false
  }

  componentWillReceiveProps(newProps) {
    this.forceUpdate()
    // Did the login attempt complete?
    if (this.isAttempting && !newProps.fetching) {
      this.props.navigation.navigate('LoginScreen')
    }
  }

  componentWillMount() {
    // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
    // TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  keyboardDidShow = (e) => {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    let newSize = Metrics.screenHeight - e.endCoordinates.height
    this.setState({
      visibleHeight: newSize,
      topLogo: {width: 100, height: 70}
    })
  }

  keyboardDidHide = (e) => {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      visibleHeight: Metrics.screenHeight,
      topLogo: {width: Metrics.screenWidth}
    })
  }

  handlePressSignup = () => {
    const {username, firstname, lastname, email, password} = this.state
    const data = {
      username:username,
      firstname: firstname,
      lastname: lastname,
      email: email,
      password: password
    }
    this.isAttempting = true
    this.props.attemptSignup(data)
  }

  handleChangeUsername = (text) => {
    this.setState({username: text})
  }
  handleChangeFirstname = (text) => {
    this.setState({firstname: text})
  }
  handleChangeLastname = (text) => {
    this.setState({lastname: text})
  }
  handleChangeEmail = (text) => {
    this.setState({email: text})
  }
  handleChangePassword = (text) => {
    this.setState({password: text})
  }

  render() {
    const {username, password, firstname, lastname, email} = this.state
    const {fetching} = this.props
    const editable = !fetching
    const textInputStyle = editable ? styles.textInput : styles.textInputReadonly
    return (
      <ScrollView contentContainerStyle={{justifyContent: 'center'}}
                  style={[styles.container, {height: this.state.visibleHeight}]} keyboardShouldPersistTaps='always'>
        <View style={styles.form}>
          <View style={styles.row}>
            <Text style={styles.rowLabel}>Username</Text>
            <TextInput
              ref='username'
              style={textInputStyle}
              value={username}
              editable={editable}
              keyboardType='default'
              returnKeyType='next'
              autoCapitalize='none'
              autoCorrect={false}
              onChangeText={this.handleChangeUsername}
              underlineColorAndroid='transparent'
              onSubmitEditing={() => this.refs.firstname.focus()}
              placeholder='Username'/>
          </View>
          <View style={styles.row}>
            <Text style={styles.rowLabel}>First name</Text>
            <TextInput
              ref='firstname'
              style={textInputStyle}
              value={firstname}
              editable={editable}
              keyboardType='default'
              returnKeyType='next'
              autoCapitalize='none'
              autoCorrect={false}
              onChangeText={this.handleChangeFirstname}
              underlineColorAndroid='transparent'
              onSubmitEditing={() => this.refs.lastname.focus()}
              placeholder='First name'/>
          </View>
          <View style={styles.row}>
            <Text style={styles.rowLabel}>Last name</Text>
            <TextInput
              ref='lastname'
              style={textInputStyle}
              value={lastname}
              editable={editable}
              keyboardType='default'
              returnKeyType='next'
              autoCapitalize='none'
              autoCorrect={false}
              onChangeText={this.handleChangeLastname}
              underlineColorAndroid='transparent'
              onSubmitEditing={() => this.refs.email.focus()}
              placeholder='Last name'/>
          </View>
          <View style={styles.row}>
            <Text style={styles.rowLabel}>E-mail</Text>
            <TextInput
              ref='email'
              style={textInputStyle}
              value={email}
              editable={editable}
              keyboardType='default'
              returnKeyType='next'
              autoCapitalize='none'
              autoCorrect={false}
              onChangeText={this.handleChangeEmail}
              underlineColorAndroid='transparent'
              onSubmitEditing={() => this.refs.password.focus()}
              placeholder='E-mail'/>
          </View>
          <View style={styles.row}>
            <Text style={styles.rowLabel}>Password</Text>
            <TextInput
              ref='password'
              style={textInputStyle}
              value={password}
              editable={editable}
              keyboardType='default'
              returnKeyType='go'
              autoCapitalize='none'
              autoCorrect={false}
              secureTextEntry
              onChangeText={this.handleChangePassword}
              underlineColorAndroid='transparent'
              onSubmitEditing={this.handlePressLogin}
              placeholder='Password'/>
          </View>
          <View style={[styles.loginRow]}>
            <TouchableOpacity style={styles.loginButtonWrapper} onPress={this.handlePressSignup}>
              <View style={styles.loginButton}>
                <Text style={styles.loginText}>Sign Up</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.loginButtonWrapper} onPress={() => this.props.navigation.goBack()}>
              <View style={styles.loginButton}>
                <Text style={styles.loginText}>Go back</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    fetching: state.signup.fetching,
    user: state.signup.user,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptSignup: (data) => dispatch(SignupActions.signupRequest(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupScreen)
