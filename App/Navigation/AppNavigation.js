import {StackNavigator, DrawerNavigator} from 'react-navigation'
import LaunchScreen from '../Containers/LaunchScreen'
import LoginScreen from '../Containers/LoginScreen'
import ProfileScreen from '../Containers/ProfileScreen'
import SignupScreen from '../Containers/SignupScreen'
import UsersListScreen from '../Containers/UsersListScreen'
import {Text} from 'react-native'
import React from 'react'
import DrawerContainer from '../Components/DrawerContainer'
import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  LaunchScreen: {screen: LaunchScreen},
  LoginScreen: {
    screen: LoginScreen,
    navigationOptions: {title: 'Login'}
  },
  SignupScreen: {
    screen: SignupScreen,
    navigationOptions: {title: 'SignupScreen'}
  },
  ProfileScreen: {
    screen: ProfileScreen,
    navigationOptions: {title: 'Profile'}
  },
  UsersListScreen: {
    screen: UsersListScreen,
    navigationOptions: {title: 'Users'}
  },
}, {
  // Default config for all screens
  // headerMode: 'none',
  // initialRouteName: 'LaunchScreen',
  // title: 'Main',
  // navigationOptions: {
  //   headerStyle: {backgroundColor: '#E73536'},
  //   title: 'You are not logged in',
  //   headerTintColor: 'white'
  // }
  headerMode: 'float',
  initialRouteName: 'LaunchScreen',
  navigationOptions: ({navigation}) => ({
    headerLeft: <Text onPress={() =>navigation.navigate('DrawerStack')}>Menu</Text>,
    headerStyle: {backgroundColor: '#4C3E54'},
    title: 'Welcome!',
    headerTintColor: 'white',
  })
})
const DrawerStack = DrawerNavigator({
  ProfileScreen: { screen: ProfileScreen },
  LoginScreen: { screen: LoginScreen },
  UsersListScreen: { screen: UsersListScreen },
},{
  gesturesEnabled: false,
  contentComponent: DrawerContainer
})
export default PrimaryNav
