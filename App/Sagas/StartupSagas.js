import { put, select, call } from 'redux-saga/effects'
import StartupActions from '../Redux/StartupRedux'
import { is } from 'ramda'
import {AsyncStorage} from 'react-native';
import {api} from '../Config/apiConfig'

// exported to make available for tests
export const selectAvatar = (state) => state.github.avatar

// process STARTUP actions
export function * startup (action) {
  if (__DEV__ && console.tron) {
    // straight-up string logging
    console.tron.log('Hello, I\'m an example of how to log via Reactotron.')

    // logging an object for better clarity
    console.tron.log({
      message: 'pass objects for better logging',
      someGeneratorFunction: selectAvatar
    })

    // fully customized!
    const subObject = {a: 1, b: [1, 2, 3], c: true}
    subObject.circularDependency = subObject // osnap!
    console.tron.display({
      name: '🔥 IGNITE 🔥',
      preview: 'You should totally expand this',
      value: {
        '💃': 'Welcome to the future!',
        subObject,
        someInlineFunction: () => true,
        someGeneratorFunction: startup,
        someNormalFunction: selectAvatar
      }
    })
  }
  const avatar = yield select(selectAvatar)
  // only get if we don't have it yet
  // if (!is(String, avatar)) {
  //   yield put(GithubActions.userRequest('GantMan'))
  // }
}


export function* getUserRequest(action) {
  const token = yield AsyncStorage.getItem('@Token') || ''
  const response = yield api.get(`/profile`, {
    'Authorization': `${token}`,
  })
  if (response.ok) {
    console.log('response OK', response)
    const user = response.data.user
    yield put(StartupActions.getUserSuccess(user))
  } else {
    yield put(StartupActions.getUserFailure("WRONG"))
  }
}
