import { put } from 'redux-saga/effects'
import SignupActions from '../Redux/SignupRedux'

// attempts to login
export function * signup (user) {
  if (user.password === '') {
    // dispatch failure
    yield put(SignupActions.signupFailure('WRONG'))
  } else {
    // dispatch successful logins
    yield put(SignupActions.signupSuccess(user))
  }
}
