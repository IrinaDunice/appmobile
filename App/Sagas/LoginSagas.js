import { put,call } from 'redux-saga/effects'
import LoginActions from '../Redux/LoginRedux'
import {api} from '../Config/apiConfig'

function postCredentialsApi(username, password) {
  return api.post('/login', {username: username, password: password})
    .then(response => ({response}))
    .catch(error => ({ error }))
}

export function* login({ username, password }) {
  const { response, error } = yield postCredentialsApi(username, password)
  if (response) {
    yield put(LoginActions.loginSuccess(response))
  }
  else {
    yield put(LoginActions.loginFailure('WRONG'))
  }
}
