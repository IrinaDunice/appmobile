import {put} from 'redux-saga/effects'
import UsersActions from '../Redux/UsersRedux'
import {AsyncStorage} from 'react-native';
import {api} from '../Config/apiConfig'


export function* getAllUsers(action) {
  console.log('getAllUsers')
  const token = yield AsyncStorage.getItem('@Token') || ''
  const response = yield api.get(`/users`, {
      'Authorization': `${token}`,
  })

  console.log('response', response)
  if (response.ok) {
    console.log('response OK', response)
    const users = response.data.users
    yield put(UsersActions.getUsersSuccess(users))
  } else {
    yield put(UsersActions.getUsersFailure("WRONG"))
  }
}
