import {put} from 'redux-saga/effects'
import ProfileActions from '../Redux/ProfileRedux'
import {api} from '../Config/apiConfig'
import {AsyncStorage} from 'react-native';

function getUserInfo(id) {
  let token = AsyncStorage.getItem('@Token')
  return api.get(`/profile`,
    {
      headers:
        {'Authorization': `Bearer ${token}`}
    })
    .then(response => ({response}))
    .catch(error => ({error}))
}

export function* fetchUserProfile() {
  if (id === '') {
    yield put(ProfileActions.fetchUserFailure('WRONG'))
  } else {
    const {response, error} = yield getUserInfo()
    if (response) {
      yield put(ProfileActions.fetchUserSuccess(response))
    } else {
      yield put(ProfileActions.fetchUserFailure(error))
    }
  }
}
