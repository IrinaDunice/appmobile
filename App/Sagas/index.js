import {takeLatest} from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import {StartupTypes} from '../Redux/StartupRedux'
import {GithubTypes} from '../Redux/GithubRedux'
import {LoginTypes} from '../Redux/LoginRedux'
import {SignupTypes} from '../Redux/SignupRedux'
import {ProfileTypes} from '../Redux/ProfileRedux'
import {UsersTypes} from '../Redux/UsersRedux'

/* ------------- Sagas ------------- */

import {startup, getUserRequest} from './StartupSagas'
import {login} from './LoginSagas'
import {signup} from './SignupSagas'
import {fetchUserProfile} from './ProfileSagas'
import {getUserAvatar} from './GithubSagas'
import {getAllUsers} from './UsersSagas'
/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield [
    // some sagas only receive an action
    takeLatest(StartupTypes.GET_USER_REQUEST, getUserRequest),
    takeLatest(LoginTypes.LOGIN_REQUEST, login),
    takeLatest(SignupTypes.SIGNUP_REQUEST, signup),
    takeLatest(ProfileTypes.FETCH_USER_REQUEST, fetchUserProfile),
    takeLatest(UsersTypes.GET_USERS_REQUEST, getAllUsers),



    // some sagas receive extra parameters in addition to an action
    takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, api)
  ]
}


